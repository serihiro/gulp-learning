path               = require 'path'

module.exports =
  # Specify entry file.
  # If you specify with key, the key is used as [name] in output.
  # You can specify multiple entry point.
  # eg1:
  #  entry:
  #    page1: './src/page1.coffee'
  #    page2: './src/page2.coffee'
  #
  # eg2:
  #   entry: ['./src/hoge.coffee', './src/foo.coffee']
  #
  # You can also omit key name. Then the key is set as 'main' by default.
  # eg:
  #   entry: './src/app.coffee'
  entry:
    hoge: './src/app.coffee'

  # Specify module loader.
  # In this sample, files that matched /\.coffee$/ will be loaded as coffee-script.
  module:
    loaders: [
      {test: /\.coffee$/, loader: 'coffee-loader'}
    ]

  # Specify output file directory and name.
  output:
    path: path.join(__dirname, 'dist')
    filename: '[name].bundle.js'

  # Specify file path resolve setting.
  resolve:
    root: [path.join(__dirname, 'src')] # module directory root directory
    extensions: ['', '.coffee', '.webpack.js', '.web.js', '.js'] # file extensions that you want to omit in source code.
