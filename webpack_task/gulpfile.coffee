gulp = require 'gulp'
$    = do require 'gulp-load-plugins'

# src内のファイルを対象に、webpackを通してdistディレクトリにコンパイルします
gulp.task 'webpack', ->
  gulp.src './src/*.coffee'
  .pipe $.webpack require './webpack.config.coffee'
  .pipe $.uglify()
  .pipe gulp.dest './dist/'

# ファイルの監視を設定
gulp.task 'default', ->
  $.watch './src/**/*.+(coffee|js)', ->
    gulp.start 'webpack'
