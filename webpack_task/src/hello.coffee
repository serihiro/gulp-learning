'use strict'

class Hello
  constructor: (name) ->
    @name = name

  greet: ->
    @name
