'use strict'

gulp      = require('gulp') # gulp
sass      = require('gulp-sass') # build sass
pleeease  = require('gulp-pleeease') # add vender prefix
rename    = require('gulp-rename') # rename output file
concat    = require('gulp-concat') # minify output files
del       = require('del') # delete file

gulp.task 'sass:develop', ->
  # say goodbye to all ./css/*.css files.
  # In this task, this is not nessesary, but I think when developing with using 'watch' this is nessesary.
  del(['./css/*.css'])
  gulp.src('sass/**/*.scss') # specify build target files
      .pipe(sass()) # convert css to sass
      .pipe(pleeease()) # add vender prefix
      .pipe(rename({ suffix: '.min' })) # add suffix to built css file
      .pipe(gulp.dest('./css')) # output .css files

gulp.task 'sass:production', ->
  del(['./css/*.css']) # say goodbye to all ./css/*.css files
  gulp.src('sass/**/*.scss') # specify build target files
      .pipe(sass()) # convert css to sass
      .pipe(pleeease()) # add vender prefix
      .pipe(concat('build.css')) # minify out css files
      .pipe(gulp.dest('./css'))
