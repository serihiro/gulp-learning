'user strict'

sub = require './sub'

class Hello
  constructor: (name) ->
    @name = name

  greet: ->
    "hello, #{name}!"
    sub name

window.Hello = Hello

hello = new Hello('naoya-san')
hello.greet()
