gulp        = require 'gulp'
$           = require('gulp-load-plugins')()
browserify  = require 'browserify'
source      = require 'vinyl-source-stream'
buffer      = require 'vinyl-buffer'

gulp.task 'browserify', ->
   # browserify is seems that we can't use regular expression file specification for 'entries'.
   # So we must specify source files with abstract path :(
   # To avoid this some solusions were found in google, but didn't work...
   # e.g. http://takahashifumiki.com/web/programing/3512/
  browserify
    entries: ['src/hello.coffee']
    transform: ['coffeeify']
    extensions: ['.coffee']
  .bundle()
  .pipe source 'bundle.js'
  .pipe buffer()
  .pipe $.sourcemaps.init
    loadMaps: true
  .pipe $.uglify()
  .pipe $.sourcemaps.write('../maps')
  .pipe gulp.dest './dist'
