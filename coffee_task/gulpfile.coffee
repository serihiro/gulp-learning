'ues strict'

gulp = require('gulp') # gulp
$    = require('gulp-load-plugins')() # load all gulp plugin in package.json

gulp.task 'coffee:build', ->
  gulp.src './src/*.coffee'
      .pipe $.sourcemaps.init # add sourcemap to build.js
        loadMaps: true
      .pipe $.coffee()
      .pipe $.concat('build.js')
      .pipe $.uglify()
      .pipe $.sourcemaps.write()
      .pipe gulp.dest('./dist')
