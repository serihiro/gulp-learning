'user strict'

class Hello
  constructor: (name) ->
    @name = name

  greet: ->
    "hello, #{name}!"

window.Hello = Hello

hello = new Hello('naoya-san')
hello.greet()
